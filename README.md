# Wooden - chat
Twitch chat app napsaná pomocí Reactu  
**Knihovny:** 
 - #### `Material-UI` 
    - Knihovna React omponentů, které odpovídají Google Material Design Guidelines
 - #### `Axios`
    - Jeden z nejznámnějších JS HTTP clientů
 - #### `Color`
    - Knihovna pro operaci s barvami
    - Používám pro úpravu barev uživatelů aby byly dobře viditelné
 - #### `Immutability Helper`
    - Nástroj pro transformaci JS objektů
 - #### `Interweave`
    - Nástroj pro 'escapování' a parsování odkazů
    - Používám pro všechny zprávy
 - #### `JS Cookie`
    - Lehká knihovna pro práci s Cookies
 - #### `React Router`
    - Nejznámnější React routovací knihovna
 - #### `Redux`
    - jeden z nejznamnějších state managerů s React modulem
    - Používám na udržování stavu o chat místnostech, uživateli a UI
 - #### `Rx JS`
    - JS verze Rx 'technologie'
    - Prakticky nevyužívám (jen client status observable)
 - #### `Tmi.js` 
    - Twitch messaging interface
    - Knihovna pro komunikaci s Twitch IRC (Internet Relay Chat)
    - Využívá sockety pro kominikaci s Twitchem
    - Pro odesílání a přijímání zpráv (a notifikací)
    
    
## Příprava
Před tímto projektem jsem už s knihovnou Tmi.js pracoval na Chat-Botovi napsaném v Kotlin/JS.
Řekl jsem si tedy, že by nemusel být takový problém tuto knihovnu využít na tvorbu chatu. Twitch
je přeci jenom taky napsaný v Reactu a využíva stejný IRC.  
S reactem samotným to už bylo horší, protože jsem s ním (a ani s jiným frameworkem založeným
na deklarativní UI) nikdy nepracoval a tak mi chvilku trvalo, než jsem se do toho vžil. Po pár
desítkách hodin už jsem se začal orientovat a měl jsem základ chatu.
## Průběh
Celkem dost času mi zabralo parsování zpráv a odzanků uživatele. Každá zpráva totiž obsahuje 
pole s výskytem emotikonů a s id toho emotikonu. Je třeba tedy zprávu správně rozřezat a vyměnit
klíčová slova za emotikony. Dalším problémem jsou odznaky uživatelů u kterých jsem dlouho hledal
nějaký seznam všech možných odznaků, ale po chvíli jsem to vzdal a nic nenašel. Vykradl jsem tedy
nějaký jiný projekt, ale ani teď nemám všechny.  
Když jsem měl zprávy hotové, vrhnul jsem se na UI samotné. Tak trochu jsem okopíroval Discord
UI a přidal jsem side bar s detaily o aktuální místnosti. 'Chatters' statistika stále nefunguje
úplně spolehlivě, ale jinak by to mělo být celkem stabilní.  
Emotikon drawer v chatu (vespod) je asi nejhorší část aplikace, jelikož se při otevření načte
přes 300 emotikonů. Celé menu s emotikony by chtělo předělat. Musel jsem ho dokonce manuálně
skrýt když je zavřené, protože pak byla aplikace na slabších mobilních zařízeních prakticky
nepoužitelná. 
## Závěr
Myslím, že se aplikace celkově povedla, ale kvůli mé nezkušenosti s Reactem (a vlastně
i Javascriptem) je občas trochu zasekaná a při připojení k opravdu velké chat místnosti začne
být špatná optimalizace opravdu viditelná.
Aplikace je deploynutá pomocí Surge na adrese https://woodenchat.surge.sh/.  
  
***Pro přihlášení je třeba Twitch account.***  