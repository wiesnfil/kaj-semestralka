
export const SET_ROOM = "SET_ROOM";
export const SET_ROOM_BADGES = "SET_ROOM_BADGES";
export const SET_CHANNEL = "SET_CHANNEL";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const SET_USER = "SET_USER";
export const SET_USER_EMOTES = "SET_USER_EMOTES";
export const TOGGLE_CHANNEL_INFO = "TOGGLE_CHANNEL_INFO";