import {clientId} from "../../resources/constants";
import {setChannel, setRoomBadges} from "../redux/actions";
import store from "../redux/store";
import axios  from "axios";

export function fetchBadges(room) {
    axios.get(`https://badges.twitch.tv/v1/badges/channels/${room.id}/display`)
        .then(res => {
            const sets = res.data.badge_sets.subscriber;
            if (sets) store.dispatch(setRoomBadges(room.name, sets.versions))
        });
}

export function fetchChannel(id) {
    axios.get(`https://api.twitch.tv/helix/users`, {
        params: { id: id },
        headers: { "Client-ID": clientId }
    }).then(res => store.dispatch(setChannel(res.data.data[0])));
}

export async function fetchChannelByName(name) {
    let result = null;

    await axios.get("https://api.twitch.tv/helix/users", {
        params: { login: name },
        headers: { "Client-ID": clientId }
    }).then(res => result = res.data.data[0]);

    return result;
}
export function fetchEmotes(room) {

}