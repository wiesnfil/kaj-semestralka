import axios from "axios"
import {clientId} from "../../resources/constants";
import store from "../redux/store";
import {setUserEmotes} from "../redux/actions";
import update from 'immutability-helper';


export function fetchEmotes(emotes) {
    let emoteData = {};

    // Fetch emotes
    axios.get("https://api.twitch.tv/kraken/chat/emoticon_images", {
        params: { emotesets: emotes },
        headers: { "Client-ID": clientId }

        // Retrieve emotes from received data
    }).then(response => Object
        .entries(response.data["emoticon_sets"])

        // Save emotes and fetch emote-set metadata
    ).then(data => {
        for (const set of data)
            emoteData[set[0]] = set[1];
        return axios.get("https://api.twitchemotes.com/api/v4/sets", {
            params: { id: emotes },
        })

        // Join set names and emotes and dispatch redux action
    }).then(({ data }) => {
        let resObj = {};

        for (const set of data)
            resObj[set["channel_name"]] = [];

        for (const set of data) resObj = update(resObj, {
            [set["channel_name"]] : {$push: emoteData[set["set_id"]]}
        });

        // Store to global state
        store.dispatch(setUserEmotes(resObj))
    }).catch(err => console.error(err.data))
}