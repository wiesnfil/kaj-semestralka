import {
    CONNECTED_STATUS,
    CONNECTING_STATUS,
    DISCONNECTED_STATUS,
    FIRST_CONNECTING,
    Tmi,
    TmiProvider
} from "./TmiProvider";
import { Message } from "./entities/Message";
import store from "./redux/store";
import {addMessage, setRoom, setUser} from "./redux/actions";
import {Room} from "./entities/Room";
import {fetchBadges, fetchChannel} from "./twitch/Rooms";
import {fetchEmotes} from "./twitch/Emotes";

export default class ChatChannels {
    constructor() {
        this.canConnect = false;

        Tmi.statusSubject.subscribe(status => {
            console.log("Status: " + status);
            switch (status) {
                case FIRST_CONNECTING: this._setup(); break;
                case CONNECTING_STATUS: break;
                case CONNECTED_STATUS: this.canConnect = true; break;
                case DISCONNECTED_STATUS: this.canConnect = false; break;
                default:break;
            }
        });
    }

    // Setup all listeners
    _setup = () => {
        const client = Tmi.client;
        client.on("chat", this.onChat);
        client.on("action", this.onAction);
        client.once("roomstate", this._setupUserState);
        client.on("roomstate", this._onConnectedToRoom);
        client.on("messagedeleted", ChatChannels._onMessageDeleteConfig);
        client.on("notice", ChatChannels._onMessageDeleteConfig);
    };

    // "Tmi.client.globaluserstate" is guaranteed to be loaded after first room load -> save it to global state and fetch emotes
    _setupUserState = () => {
        store.dispatch(setUser(Tmi.client.globaluserstate));
        fetchEmotes(Tmi.client.emotes);
    };

    // When connected to room, fetch badges and channel info and save to global state
    _onConnectedToRoom = (channel, state) => {
        const room = new Room(channel, state["room-id"]);
        store.dispatch(setRoom(room));

        fetchBadges(room);
        fetchChannel(room.id)
    };

    // On chat create Message data class and save it to global state
    onChat = (channel, userstate, message, self, action = false) => {
        const newMsg = new Message(channel, userstate, message, self, action);
        store.dispatch(addMessage(newMsg));
        return newMsg;
    };

    // Action is message where message is colored according to user color
    onAction = (channel, userstate, message, self) => {
        return this.onChat(channel, userstate, message, self, true)
    };

    join = (name) => {
        const join = channel => {
            // try join channel
            Tmi.client.join(channel)
                .then(() =>  TmiProvider.saveChannelState() )
                .catch(() => TmiProvider.saveChannelState() );
        };

        // Connect after TMI is ready
        const setupLateConnect = () => {
            Tmi.statusSubject.subscribe(status => {
                if (status === CONNECTED_STATUS) join(name)
            });
        };

        // connect if can or schedule later connect
        if (this.canConnect) {
            if (ChatChannels.isNotConnected(name))
                join(name);
        } else setupLateConnect()
    };

    disconnect = (name) => {
        // if connected leave chat room and synchronize rooms state
        if (ChatChannels.isConnectedTo(name) && this.canConnect)
            Tmi.client.part(name).then(()=>
                TmiProvider.saveChannelState()
            );
    };

    static isConnectedTo(channel) {
        if (Tmi.client) return Tmi.client
            .getChannels()
            .map(ele => {
                if (channel[0] === `#`) return ele;
                else return ele.substring(1);
            }).some(ele => ele === channel);
        else return false;
    }
    static isNotConnected(channel) { return !ChatChannels.isConnectedTo(channel) }

    // == Utility ==

    static _onMessageDeleteConfig(channel, username, deletedMessage, userstate) {
        console.log(`Deleted ${deletedMessage}`);
    }

    static _onNoticeReceived(channel, msgid, message) {
        console.log(`Received notice: ${msgid}`)
    }

    static _fetchEmotes() { // Does not work. Lib fault?
        console.log(Tmi.client.emotes/*.globaluserstate["emote-sets"]*/);
    }
}

export const channels = new ChatChannels();