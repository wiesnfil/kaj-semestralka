import {useEffect, useState} from 'react';
import axios, { CancelToken } from 'axios';
import store from "../redux/store";
import { unstable_useMediaQuery as useMediaQuery } from '@material-ui/core/useMediaQuery';
import {toggleChannelInfo} from "../redux/actions";
import { CONNECTED_STATUS, CONNECTING_STATUS, Tmi } from "../TmiProvider";

const source = CancelToken.source();
const proxy = "https://cors-anywhere.herokuapp.com/";

const blankChatters = {
    chatter_count: 0,
    chatters: {
        broadcaster: [], vip: [], moderators: [], staff: [], admins: [], global_mods: [], viewers: []
    }
};

// Chatter array hook for chat room
export function useChatters(roomName) {
    const [chatters, setChatters] = useState(blankChatters);

    useEffect(()=> {
        setChatters(blankChatters);

        const fetch = () => axios
            .get(proxy + `https://tmi.twitch.tv/group/user/${roomName}/chatters`, {
                cancelToken: source.token
            })
            .then(res => {if (res)
                setChatters(res.data)
            }).catch(()=>{});

        fetch(); // Fetch chatters every minute
        const interval = setInterval(fetch,60 * 1000);

        return () => {
            source.cancel();
            window.clearInterval(interval);
        }
    },[roomName]);

    return [chatters.chatter_count, chatters.chatters];
}

// Logged user motes hook
export function useEmotes() {
    const [emotes, setEmotes] = useState(store.getState().user.emotes);

    useEffect(()=> store.subscribe(() =>
        setEmotes(store.getState().user.emotes)
    ),[]);

    return [
        // Emotes object
        emotes,

        // Emote set names
        Object.keys(emotes)
            .sort((a, b) => {
                if(a < b) { return -1; }
                if(a > b) { return 1; }
                return 0;
            })
    ]
}

// Hook for chat room info displayed status
export function useChannelInfoShown() {
    const xs = useMediaQuery('(max-width: 599px)'); // bool -> if width if lower than 600px

    let [shown, setShown] = useState(
        store.getState().ui.infoBarShown // init according to global state
    );

    useEffect(()=> store.subscribe(() =>
        setShown(store.getState().ui.infoBarShown) //  reflect changes in global state
    ),[]);

    useEffect(()=>{
        if (xs) store.dispatch(toggleChannelInfo(false)) //  reflect changes of screen width
    },[xs]);

    return shown;
}

export function useConnected() {
    const [connected, setConnected] = useState(Tmi.networkStatus === CONNECTED_STATUS);
    const [connecting, setConnecting] = useState(Tmi.networkStatus);

    useEffect(() => {
        const sub = Tmi.statusSubject.subscribe(event => {
            setConnected(event === CONNECTED_STATUS);
            setConnecting(event === CONNECTING_STATUS);
        });

        return () => sub.unsubscribe()
    });

    return [ connected, connecting ]
}