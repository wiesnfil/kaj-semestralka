const Colors = require('color');

const default_colors = [
    "#E57373",
    "#F06292",
    "#CE93D8",
    "#B39DDB",
    "#9FA8DA",
    "#90CAF9",
    "#29B6F6",
    "#26C6DA",
    "#4DB6AC",
    "#81C784",
    "#AED581",
    "#DCE775",
    "#FDD835",
    "#FFB300",
    "#FB8C00",
];

// Choose color tor username
const pickColor = function(name) {
    const n = name.charCodeAt(0) + name.charCodeAt(name.length - 1);
    return default_colors[n % default_colors.length];
};

// calculate color according user setting and background color
export function colorForUser(userstate, backgroundColor) {
    const back = Colors(backgroundColor); // background color
    let color = userstate.color; // user username color

    if (!color) color = pickColor(userstate.username); // if user does not have color, choose one

    if (color === "#000000") // forbid pure black
        color = "#010101";
    else if (color.toUpperCase() === "#FFFFFF") // forbid pure white
        color = "#FEFEFE";

    color = Colors(color); // If color is dark, lighten it and if light, darken it
    if (back.isDark()) while (color.contrast(back) < 4)
        color = color.lighten(0.05);
    else while (color.contrast(back) < 4)
        color = color.darken(0.05);

    return color.hex(); // return hex representation
}

