import {
    ADD_MESSAGE,
    SET_CHANNEL,
    SET_ROOM,
    SET_ROOM_BADGES,
    SET_USER,
    SET_USER_EMOTES, TOGGLE_CHANNEL_INFO
} from "../../resources/ReduxConstants";
import update from 'immutability-helper';

const initialState = {
    // Rooms containing id, name, badges, emotes and messages
    storedRooms: {},

    // Information about channels: type, description, name, id, image url and view count
    channels: {},

    // User emotes
    user: {
        id: 0,
        displayName: "",
        color: "#888",
        emotes: []
    },

    // UI data: channel info toggle
    ui: {
        infoBarShown: true
    }
};
function rootReducer(state = initialState, action) {
    switch (action.type) {

        case SET_CHANNEL: {
            return update(state, {
                channels: {
                    [action.payload.login]: {$set: action.payload}
                }
            })
        }

        case SET_ROOM: {
            const room = state.storedRooms[action.payload.name];
            let messages = [];
            if (room) messages = room.messages;
            action.payload.messages = messages;
            return update(state, {
                storedRooms: {
                    [action.payload.name]: {$set: action.payload},
                }
            })
        }

        case SET_ROOM_BADGES: {
            return update(state, {
                storedRooms: {
                    [action.payload.room]: {
                        subBadges: {$set: action.payload.badges}
                    }
                }
            })
        }

        case SET_USER: {
            const userData = action.payload;
            return update(state, {
                user: {
                    id: {$set: userData["user-id"]},
                    displayName: {$set: userData["display-name"]},
                    color: {$set: userData.color}
                }
            });
        }

        case SET_USER_EMOTES: {
            const emotes = action.payload;
            return update(state, {
                user: { emotes: {$set: emotes} }
            })
        }


        case ADD_MESSAGE: {
            const name = action.payload.channel.substring(1);
            return update(state, {
                storedRooms: {
                    [name]: {
                        messages: {$push: [action.payload]}
                    }
                }
            })
        }

        case TOGGLE_CHANNEL_INFO: {
            return update(state, {
                ui: {
                    infoBarShown: {
                        $set: action.payload ? action.payload : !state.ui.infoBarShown
                    }
                }
            })
        }

        default: return state
    }
}

export default rootReducer;