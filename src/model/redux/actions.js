import {
    ADD_MESSAGE,
    SET_CHANNEL,
    SET_ROOM,
    SET_ROOM_BADGES,
    SET_USER,
    SET_USER_EMOTES, TOGGLE_CHANNEL_INFO
} from "../../resources/ReduxConstants";

export const setRoom = payload => ({
    type: SET_ROOM,
    payload: payload
});

export const setRoomBadges = (room, badges) => ({
    type: SET_ROOM_BADGES,
    payload: {
        room: room,
        badges: badges
    }
});

export const setChannel = data => ({
    type: SET_CHANNEL,
    payload: data
});

export const addMessage = payload => ({
    type: ADD_MESSAGE,
    payload: payload
});

export const setUser = user => ({
    type: SET_USER,
    payload: user
});

export const setUserEmotes = emotes => ({
    type: SET_USER_EMOTES,
    payload: emotes
});

export const toggleChannelInfo = open => ({
    type: TOGGLE_CHANNEL_INFO,
    payload: open
});