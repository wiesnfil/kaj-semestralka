
// Message data class
export class Message {
    constructor(channel, userstate, message, self, action = false) {
        this.channel = channel;
        this.userstate = userstate;
        this.message = message;
        this.self = self;
        this.action = action;
    }
}

// Message id
Message.prototype.id = function () {
    return this.userstate.id
};

// If message author is mod
Message.prototype.isUserMod = function () {
    return this.userstate.mod
};