
// Chat room data class
export class Room {
    constructor(channel, roomId) {
        this.name = channel.substring(1);
        this.id = roomId;
        this.messages = [];
        this.subBadges = {};
        this.bttvEmotes = [];
    }
}