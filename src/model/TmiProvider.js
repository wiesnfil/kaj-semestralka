import {clientId} from "../resources/constants";
import {Subject} from "rxjs";
import tmi from "tmi.js"
import Cookies from "js-cookie"

const CHANNELS_COOKIE = "channels", TOKEN_COOKIE = "password";
export const AUTH_FAILED = "auth_failed", CONNECTING_STATUS = "connecting", CONNECTED_STATUS = "connected", DISCONNECTED_STATUS = "disconnected", FIRST_CONNECTING = "first_connecting";
export class TmiProvider {
    constructor() {
        // Create RxJS subject for chat status
        this.statusSubject = new Subject();
        this.networkStatus = DISCONNECTED_STATUS;
        this.statusSubject.subscribe(state => this.networkStatus = state);

        if (TmiProvider.isLogged()) // if token in cookie, connect
            this.connect(Cookies.get(TOKEN_COOKIE));
    }

    connect(token) {
        // Save token to Cookie and connect with given token
        TmiProvider.setUser(token);
        this.client = new tmi.client({
            identity: {
                password: token,
                username: "NONE"
            },
            channels: Cookies.getJSON(CHANNELS_COOKIE),
            connection: {
                reconnect: true,
                secure: true,
            },
            options: {
                // debug: true,
                clientId: clientId
            }
        });

        // Setup status listeners
        this.client.once("connecting", () => this.statusSubject.next(FIRST_CONNECTING));
        this.client.on("connected", () => this.statusSubject.next(CONNECTED_STATUS));
        this.client.on("connecting", () => this.statusSubject.next(CONNECTING_STATUS));
        this.client.on("disconnected", () => this.statusSubject.next(DISCONNECTED_STATUS));
        this.client.connect()
            .catch(() => this.statusSubject.next(AUTH_FAILED));
    }

    // User Cookie getters/setters
    static setUser(token) { Cookies.set(TOKEN_COOKIE, token)}
    static isLogged() { return !!Cookies.get(TOKEN_COOKIE); }
    static logOut() {
        Cookies.remove(TOKEN_COOKIE);
        Cookies.remove(CHANNELS_COOKIE);
    }

    // Save state of connected rooms to Cookie
    static saveChannelState = (mutator) => {
        if (mutator) {
            Cookies.set(CHANNELS_COOKIE, mutator(
                Cookies.getJSON(CHANNELS_COOKIE)
            )) // mutate current state and save
        } else Cookies.set(CHANNELS_COOKIE, Tmi.client.getChannels());
    };

    // retrieve rooms state from cookie
    static retrieveChannelNames = () => {
        if (Cookies.get(CHANNELS_COOKIE))
            return Cookies.getJSON(CHANNELS_COOKIE)
                .map(ele => ele.substring(1));
        else return [];
    };
}

export const Tmi = new TmiProvider();