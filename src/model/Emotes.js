import React from "react";
import TwitchEmote from "../views/chat/utility/TwitchEmote";
import store from "./redux/store";
import Interweave from "interweave/lib/index";
import {UrlMatcher} from "interweave-autolink/lib/index";

// export function bttvChannel(channel) {
//     return new Promise((resolve, reject) => {
//         axios.get(`https://api.betterttv.net/2/channels/${channel}`)
//             .then(resp => {
//                 resolve(JSON.parse(resp.data).emotes)
//             }).catch(reject);
//     })
// }

// emotify = parse emotes from message metadata or logged user emotes
export function emotify(message) {
    const result = [];
    let emotes = [];
    const words = message.message.split(' '); // all words in chat message

    const forEachWord = (fun) => { // word iterator that gives word and position of that word
        let characters = 0;
        for (let i = 0; i < words.length; i++) {
            fun(words[i], characters);
            characters += Array.from(words[i]).length + 1;
        }
    };

    // if there are some emotes in message, parse them
    if (message.userstate.emotes) {
        emotes = Object
            .entries(message.userstate.emotes)
            .flatMap(em => // [ id, start-end ]
                em[1].map(emPos => {
                    const placeSplit = emPos.split('-');
                    return {id: em[0], from: parseInt(placeSplit[0])}
                })
            );
    }

    // If message is by logged user
    if (message.self) {
        // Check if emotes are loaded
        const storeEmotes = store.getState().user.emotes;
        if (storeEmotes) {
            const allEmotes = Object.values(storeEmotes).flat();
            const allEmoteKeywords = allEmotes.map(em => em.code);

            forEachWord((word, index) => {
                if (allEmoteKeywords.includes(word)) { // if word is emote
                    // find emote in users emotes
                    const emote = allEmotes.filter(em => em.code === word)[0];
                    emotes.push({
                        id: `${emote.id}`,
                        from: index
                    })
                }
            });
        }
    }

    // push text element -- Interweave parses links and escapes html elements from message
    const pushText = index => result.push(<Interweave
            key={message.userstate.id + `T${index}`}
            content={continueText.join(' ')}
            matchers={[new UrlMatcher('url')]}
        />);

    // push twitch emote element
    const pushEmote = (index, emote, word) => result.push(<TwitchEmote
        key={message.userstate.id + `E${index}`}
        id={emote.id}
        name={word}
    />);

    // const pushBttvEmote = (index, emote) => result.push(<img
    //     alt={`BTTV Emote ${emote.id}`}
    //     key={message.userstate.id + `BE${index}`}
    //     className={emoteClass}
    //     src={`https://cdn.betterttv.net/emote/${emote.id}/1x`}
    // />);

    let continueText = [];

    // parse whole message
    forEachWord((word, index) => {
        // get current emote if exists
        const emote = emotes.filter( ele =>
            ele.from === index
        )[0];

        if (emote) {
            // Push past text and reset it
            if(continueText.length > 0) {
                pushText(index);
                continueText = [];
            }
            pushEmote(index, emote, word);
        } else // Store text for later
            continueText.push(word);
    });

    // Push text if any left
    if(continueText.length > 0)
        pushText(words.length);

    return result;
}