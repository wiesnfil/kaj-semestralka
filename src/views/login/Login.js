import React, {useEffect, useState} from 'react'
import {Button, Link, TextField, Typography, withStyles} from "@material-ui/core";
import {Redirect, withRouter} from "react-router-dom";
import {AUTH_FAILED, CONNECTED_STATUS, CONNECTING_STATUS, Tmi, TmiProvider} from "../../model/TmiProvider";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = theme => ({
    root: {
        height: "100%",
        background: theme.palette.background.paper,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        zIndex: theme.zIndex.appBar,
    },
    loginWrapper: {
        margin: theme.spacing.unit * 4,
        display: "flex",
        flexDirection: "column",
    },
    mainInfo: {

    },
    okBtn:{

    }
});

function Login(props) {
    const { classes } = props;

    const [token, setToken] = useState("");
    const [loginFailed, setLoginFailed] = useState(false);
    const [inProgress, setInProgress] = useState(false);
    const [redirect, setRedirect] = useState(false);

    const onLogin = () => {
        setLoginFailed(false);
        Tmi.connect(token);
    };

    useEffect(()=>{
        const sub = Tmi.statusSubject.subscribe(event => {
            switch (event) {
                case CONNECTING_STATUS:
                    setInProgress(true);
                    break;
                case CONNECTED_STATUS:
                    setInProgress(false);
                    setRedirect(true);
                    break;
                case AUTH_FAILED:
                    TmiProvider.logOut();
                    setLoginFailed(true);
                    setInProgress(false);
                    break;
                default: break;
            }
        });
        return () => sub.unsubscribe()
    });

    let helperText = "";
    if (loginFailed) helperText = "Login failed, are you sure you copied the right token?";

    return (
        <main className={classes.root}>
            {redirect && <Redirect to={"/chat"} />}

            <div className={classes.loginWrapper}>
                <Typography className={classes.mainInfo} variant={"h6"}>
                    To use this app you must first connect your Twitch account.
                    To do that, you must go <Link color={"secondary"} target={"_blank"} href={"https://twitchapps.com/tmi/"}>here</Link>, copy
                    generated token and paste it here.
                </Typography>
                <TextField
                    className={classes.input}
                    variant={"outlined"}
                    label={"Token"}
                    margin={"dense"}
                    value={token}
                    error={loginFailed}
                    helperText={helperText}
                    onChange={event => setToken(event.target.value)}
                />
                <Button className={classes.okBtn} variant={"text"} color={"secondary"} onClick={onLogin}>
                    {inProgress && <CircularProgress size={23} color="secondary" />}
                    {!inProgress && "Login"}
                </Button>
            </div>
        </main>
    )

}


export default withRouter(
    withStyles(styles)(Login)
)