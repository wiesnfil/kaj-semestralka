import React, {useEffect, useState} from 'react';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { orange } from "@material-ui/core/colors";
import withStyles from "@material-ui/core/styles/withStyles";
import {Provider} from "react-redux";
import store from "../model/redux/store";
import ChatApp from "./chat/ChatApp";
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom"
import {AppBar, CssBaseline, Toolbar} from "@material-ui/core";
import Logout from '@material-ui/icons/ExitToApp'
import Info from '@material-ui/icons/Info'
import Typography from "@material-ui/core/Typography";
import Login from "./login/Login";
import {TmiProvider} from "../model/TmiProvider";
import IconButton from "@material-ui/core/IconButton";
import {toggleChannelInfo} from "../model/redux/actions";

const backDefault = "#2a2a2a";
const backPaper = "#3a3a3a";
const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: { main: orange["800"] },
        secondary: { main: "#f00" },
        background: {
            default: backDefault,
            paper: backPaper,
            dark: "#111",
            light: "#555"
        }
    },
    overrides: {
        MuiToolbar: {
            root: { background: backPaper }
        },
        MuiTabs: {
            scrollButtons: { color: "#fff" }
        }
    },
    typography: {
        useNextVariants: true,
    },
});

const styles = () => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        overflow: "hidden"
    },
    bar: {
    },
    title: {
        marginLeft: "5px",
        flexGrow: 1
    }
});

function App(props) {
    const { classes } = props;

    const [isLogged, setLogged] = useState(TmiProvider.isLogged);

    useEffect(() => store
        .subscribe(() =>
            setLogged(!!store.getState().user.displayName)
        )
    );

    const logOut = () => {
        TmiProvider.logOut();
        window.location.reload();
    };

    return (
        <Provider store={store}>
            <CssBaseline/>
            <MuiThemeProvider theme={theme}>

                <div className={classes.root}>

                    {/* APP BAR */}
                    <AppBar position={"static"}>
                        <Toolbar className={classes.bar}>
                            <Typography
                                className={classes.title}
                                variant="h5"
                                color="inherit"
                                onClick={()=> console.log(store.getState())}
                            >
                                Wooden Chat
                            </Typography>

                            { isLogged && <>
                                <IconButton onClick={()=> store.dispatch(toggleChannelInfo())}>
                                    <Info fontSize={"large"}/>
                                </IconButton>

                                <IconButton
                                    onClick={logOut}
                                >
                                    <Logout fontSize={"large"}/>
                                </IconButton>
                            </>}
                        </Toolbar>
                    </AppBar>

                    {/* CONTENT */}
                    <Router> <Switch>
                        <Route exact path={"/login"} component={Login} />
                        <Route path={"/chat/:room?"} component={ChatApp} />
                        <Redirect to={"/chat"}/>
                    </Switch> </Router>
                </div>

            </MuiThemeProvider>
        </Provider>
    )
}

export default withStyles(styles)(App)