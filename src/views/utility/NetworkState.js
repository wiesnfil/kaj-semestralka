import React from "react";
import {CircularProgress, Tooltip, Typography, withStyles} from "@material-ui/core";
import { useConnected } from "../../model/hooks/ChatRoomHooks";
import NoSignal from '@material-ui/icons/PortableWifiOff'

const styles = theme => ({
    root: {
        position: "relative",
        width: "50px",
        height: "50px",
    },
    tooltip: {
    },
    icon: {
        position: "absolute",
        alignSelf: "center",
        top: 8, left: 7
    },
    load: {
        position: "absolute",
        top: 0, left: 0
    }
});

function NetworkState(props) {
    const { classes } = props;

    const [connected, connecting] = useConnected();

    let message = "Network error";
    if (connecting) message = "Network error - Trying to reconnect";

    return (<>
        { !connected && <div className={classes.root}>
            <Tooltip
                className={classes.tooltip}
                placement={"top-end"}
                title={<Typography variant={"subheading"}>{message}</Typography>}
            >
                <NoSignal className={classes.icon} fontSize={"large"} color={"error"}/>
            </Tooltip>

            { connecting && <CircularProgress color={"secondary"} className={classes.load} size={50}/> }
        </div>}
    </>)
}

NetworkState.propTypes = {

};

export default withStyles(styles)(NetworkState)