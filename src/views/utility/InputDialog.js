import React, {useEffect, useState} from "react";
import {Button, CircularProgress, Dialog, DialogActions, DialogContent, TextField, withStyles} from "@material-ui/core";
import * as PropTypes from "prop-types";
import {fetchChannelByName} from "../../model/twitch/Rooms";
import BlankAvatar from "@material-ui/icons/Face";

const styles = theme => ({
    content: {
        marginBottom: "-25px"
    },
    channelImg: {
        fontSize: "3.5em",
        color: theme.palette.text.primary,
        width: "1em",
        marginLeft: "0.5em",
        borderRadius: "100%",
        backgroundSize: "contain",
        opacity: 0.8,
    },
});

let delayTimer;
function InputDialog(props) {
    const { classes, open, onConfirm, onClose } = props;

    const [dialogText, setDialogText] = useState("");
    const [foundChannel, setFoundChannel] = useState(null);
    const [inProgress, setInProgress] = useState(false);

    // Clean channel input on close/open
    useEffect(()=> setFoundChannel(null), [open]);

    // On channel input change, search for channel (with half sec throttle)
    useEffect(() => {
        clearTimeout(delayTimer);
        setFoundChannel(null);
        delayTimer = setTimeout(function() {
            if (dialogText) {
                setInProgress(true);
                fetchChannelByName(dialogText)
                    .then(found => {
                        if (found) setFoundChannel(found);
                        setInProgress(false);
                    })
                    .catch(() => setInProgress(false))
            }
        }, 500);
    },[dialogText]);

    const canConfirm = !!foundChannel;
    return (
        <Dialog
            open={open}
            onClose={onClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogContent className={classes.content}>
                <TextField
                    autoFocus
                    margin="dense"
                    label="Channel name"
                    onChange={event => setDialogText(event.target.value)}
                />
                {foundChannel && <img
                    className={classes.channelImg}
                    src={foundChannel.profile_image_url}
                    alt={"Channel name"}
                />}
                {!foundChannel && <BlankAvatar color={"inherit"} className={classes.channelImg}/>}
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} >
                    Cancel
                </Button>

                <Button
                    disabled={!canConfirm}
                    onClick={() => onConfirm(dialogText)}
                >
                    {inProgress && <CircularProgress size={23}/>}
                    {!inProgress && "Open"}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

InputDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onConfirm: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(InputDialog)