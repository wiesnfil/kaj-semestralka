import React from "react";
import * as PropTypes from "prop-types";
import store from "../../../model/redux/store";

// Parse message badge metadata and prepare badges for user
export function Badges(props) {
    const badges = Object.keys(props.badges)
        .map((badge, index) => {
            switch (badge) {
                case "admin": return (<AdminBadge key={index}/>);
                case "broadcaster": return (<BroadcasterBadge key={index}/>);
                case "moderator": return (<ModeratorBadge key={index}/>);
                case "staff": return (<StaffBadge key={index}/>);
                case "turbo": return (<TurboBadge key={index}/>);
                case "vip": return (<VipBadge key={index}/>);
                case "verified": return (<VerifiedBadge key={index}/>);
                case "premium": return (<PrimeBadge key={index}/>);
                case "sub-gifter": return (<SubGifterBadge key={index}/>);
                case "partner": return (<VerifiedBadge key={index}/>);
                case "subscriber": {
                    const badges = store.getState().storedRooms[props.channel].subBadges[props.badges.subscriber];
                    if (badges)
                        return (
                            <Badge key={index}
                                   alt={"Subscriber badge"}
                                   src={store.getState().storedRooms[props.channel].subBadges[props.badges.subscriber].image_url_1x}/>
                        );
                    else return (<Badge key={index} alt={"not available"} src={""}/>);
                }
                default: return ("")
            }
        });

    return ( <span>{badges}</span> )
}

Badges.propTypes = {
    badges: PropTypes.object.isRequired,
    channel: PropTypes.string.isRequired
};

const badgeStyle = {
    verticalAlign: "middle",
    margin: "0 3px 0 0"
};

function Badge(props) {
    return (<img
        style={badgeStyle}
        src={props.src}
        alt={props.alt}
    />)
}

Badge.propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired
};

export function StaffBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/d97c37bd-a6f5-4c38-8f57-4e4bef88af34/1"}
        alt={"Staff badge"}
    />)
}

export function AdminBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/9ef7e029-4cdf-4d4d-a0d5-e2b3fb2583fe/1"}
        alt={"Admin badge"}
    />)
}

export function BroadcasterBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/5527c58c-fb7d-422d-b71b-f309dcb85cc1/1"}
        alt={"Broadcaster badge"}
    />)
}

export function ModeratorBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/3267646d-33f0-4b17-b3df-f923a41db1d0/1"}
        alt={"Moderator badge"}
    />)
}

export function VerifiedBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/d12a2e27-16f6-41d0-ab77-b780518f00a3/1"}
        alt={"Verified badge"}
    />)
}

export function VipBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/b817aba4-fad8-49e2-b88a-7cc744dfa6ec/1"}
        alt={"VIP badge"}
    />)
}

export function TurboBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/bd444ec6-8f34-4bf9-91f4-af1e3428d80f/1"}
        alt={"Turbo badge"}
    />)
}

export function PrimeBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/a1dd5073-19c3-4911-8cb4-c464a7bc1510/1"}
        alt={"Prime badge"}
    />)
}

export function SubGifterBadge(props) {
    return (<Badge
        src={"https://static-cdn.jtvnw.net/badges/v1/4592e9ea-b4ca-4948-93b8-37ac198c0433/1"}
        alt={"Sub gifter badge"}
    />)
}
