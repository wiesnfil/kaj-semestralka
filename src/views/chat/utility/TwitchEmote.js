import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import * as PropTypes from "prop-types";
import {Tooltip} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const style = () => ({
    root: {
        verticalAlign: "middle",
        marginLeft: "3px",
        marginRight: "3px"
    },
    emoteDesc: { textAlign: "center" },
    emoteWrap: { margin: "10px 0"}
});

function TwitchEmote(props) {
    const { classes, id } = props;
    let { name, size, className } = props;

    if (!className) className = classes.root;
    if (!name) name = id;
    if (!size) size = 'normal';

    let type;
    switch (size) {
        case 'normal': type = "1.0"; break;
        case 'big':    type = "1.5"; break;
        case 'huge':   type = "2.0"; break;
        default: break;
    }
    return (
        <Tooltip placement={"top"} classes={{ tooltip: classes.emoteWrap }} title={
            <div className={classes.emoteDesc}>
                <Typography variant={"caption"}>{name}</Typography>
                <img style={{height: "56px", width: "56px"}}
                     alt={`Emote ${name}`}
                     src={`https://static-cdn.jtvnw.net/emoticons/v1/${id}/2.0`}
                />
            </div>
        }>
            <img className={className}
                 alt={`Emote ${name}`}
                 src={`https://static-cdn.jtvnw.net/emoticons/v1/${id}/${type}`}
            />
        </Tooltip>
    )
}

TwitchEmote.propTypes = {
    className: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    size: PropTypes.oneOf(['normal', 'big', 'huge'])
};

export default withStyles(style)(TwitchEmote)