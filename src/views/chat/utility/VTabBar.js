import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import { Fab, Paper, Typography} from "@material-ui/core";
import * as PropTypes from "prop-types";
import NetworkState from "../../utility/NetworkState";

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        height: "100%",
        margin: "0 3px 0 3px",
        display: "flex",
        flexDirection: "column",
    },
    content: {
        flexGrow: 1,
        display: "inline-flex",
        flexDirection: "column",
    },
    roomItems: {
        margin: "10px",

        background: theme.palette.background.default,
        '&:hover': { background: theme.palette.background.paper }
    },
    activeRoom: {
        '&::before': {
            content: "''",
            position: "absolute",
            left: "-115%",
            borderRadius: "5px",
            background: "#fff",
            width: "100%",
            height: "100%"
        }
    },
    itemText: {
        fontSize: "15px",
        fontWeight: 600
    },
    itemImage: {
        width: "100%",
        borderRadius: "100%",
        backgroundSize: "contain",
        opacity: 0.8,
    },
    divider: {
        height: "1.5px",
        margin: "10px 0px 3px -5px"
    },
    whispHeader: {
        textAlign: "center",
        marginLeft: "-2.5px"
    },
    network: {
        flexGrow: 0,
        alignSelf: "center",
        margin: "0 5px 15px 5px"
    }
});

function VTabBar(props) {
    const { classes, tabs, onPageChange, activePage, firstItem, lastItem, images } = props;

    const onPageSet = index => {
        // If listener exists and page clicked is not active page
        if (onPageChange && index !== activePage)
            onPageChange(tabs[index])
    };

    return (
        <Paper square className={classes.root} elevation={0} component={'nav'}>
            <div className={classes.content}>
                {firstItem && (firstItem)}
                {tabs.map((name, index) => {
                    let classN = classes.roomItems;
                    if (index === activePage) classN += ` ${classes.activeRoom}`;
                    let image = images[index];

                    return (<Fab key={name}
                                 variant="round"
                                 className={classN}
                                 size="medium"
                                 onClick={() => {onPageSet(index)}}
                    > {/* If channel image loaded display it. Otherwise display first three letters of channel name */}
                        { image && <img
                            alt={`${name} channel avatar`}
                            className={classes.itemImage}
                            src={image}
                        />}
                        { !image && <Typography className={classes.itemText}>
                            {name.substring(0, 3)}
                        </Typography>}
                    </Fab>)
                })}
                {lastItem && lastItem}
            </div>

            <span className={classes.network}>
                <NetworkState message="dsadasd" />
            </span>
        </Paper>

    )
}

VTabBar.propTypes = {
    tabs: PropTypes.array.isRequired,
    activePage: PropTypes.number.isRequired,
    images: PropTypes.array.isRequired,
    onPageChange: PropTypes.func,
    onPageClosed: PropTypes.func,
    firstItem: PropTypes.element,
    lastItem: PropTypes.element
};

export default withStyles(styles, { withTheme: true})(VTabBar)