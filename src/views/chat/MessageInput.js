import React, { useRef, useState} from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import * as PropTypes from "prop-types";
import Emote from "@material-ui/icons/InsertEmoticon"
import { IconButton } from "@material-ui/core";
import EmoteSelection from "./EmoteSelection";
import {useConnected} from "../../model/hooks/ChatRoomHooks";

const style = () => ({
    root: {

    },
    insertBox: {
        display: "flex",
        margin: "0 5px"
    },
    emotesBtn: {
        alignSelf: "center",
        marginLeft: "5px",
        fontSize: "20px",
    },
});

function MessageInput(props) {
    const { classes, onMessage } = props;

    const [inputText, setInputText] = useState("");
    const [emotesOpen, setEmotesOpen] = useState(false);
    const [ connected ] = useConnected();
    const input = useRef(null);

    const inputKeyPress = event => {
        // Send message only if it is not blank
        if (event.key === "Enter" && inputText.trim() !== "") {
            onMessage(inputText);
            setInputText("");
        }
    };
    const inputTextChange = event => {
        // Ignore line breaks
        if (event.nativeEvent.inputType !== "insertLineBreak")
            setInputText(event.target.value);
    };

    const toggleEmotes = () => setEmotesOpen(prev => !prev);

    const onEmoteClicked = emote => {
        // Insert selected emote to input
        setInputText(prev => prev.trim() + ` ${emote} `);
        //Focus input so that user can continue writing
        input.current.focus();
    };

    let helperText;
    if (!connected) helperText = "Chat is not available while you are offline";

    return (
        <div className={classes.root}>
            <div className={classes.insertBox}>
                <TextField
                    multiline fullWidth
                    inputRef={input}
                    disabled={!connected}
                    margin="none"
                    variant="outlined"
                    value={inputText}
                    rowsMax={5}
                    error={!connected}
                    helperText={helperText}
                    onChange={e=>inputTextChange(e)}
                    onKeyPress={e=>inputKeyPress(e)}
                />
                <div className={classes.emotesBtn}>
                    <IconButton onClick={toggleEmotes} >
                        <Emote color={"action"} fontSize={"large"}/>
                    </IconButton>
                </div>
            </div>
            <EmoteSelection
                open={emotesOpen}
                onEmoteClicked={onEmoteClicked}
            />
        </div>
    )
}

MessageInput.propTypes = {
    onMessage: PropTypes.func.isRequired
};

export default withStyles(style)(MessageInput)