import React, {useEffect, useRef} from 'react';
import { withStyles } from '@material-ui/core/styles/index';
import ChatMessage from "./ChatMessage";
import {connect} from "react-redux";
import MessageInput from "./MessageInput";
import takeRight from "lodash/takeRight"

const styles = theme => ({
    root: {
        height: "100%",
        display: "flex",
        flexDirection: "column",
        backgroundColor: theme.palette.background.default,
        borderTopLeftRadius: "20px",
        borderTopRightRadius: "20px",
        boxShadow: "0px 0px 5px 0px rgba(0,0,0,0.75)",
    },
    chatBox: {
        // padding: "0 5px 0 10px",
        margin: "0px 5px 0px 5px",
        padding: "5px",
        flexGrow: 1,
        overflowY: "scroll",
    },
    input: {
        flexGrow: 0,
        marginBottom: theme.spacing.unit,
    }
});

function ChatRoom(props) {
    const { classes, room, client, storedRooms} = props;

    const chatBox = useRef(null);

    let messages = [];
    if (storedRooms[room]) messages = storedRooms[room].messages;

    const scrollToBottom = () =>
        chatBox.current.scrollTop = chatBox.current.scrollHeight - chatBox.current.clientHeight;

    // Scroll to bottom after opening chat room
    useEffect(() => { scrollToBottom(); },[room]);

    useEffect(() => {
        // After new message scroll to bottom (only if not scrolled 500 px up)
        const elem = chatBox.current;
        const isScrolledToBottom = elem.scrollHeight - elem.clientHeight <= elem.scrollTop + 500;
        if (isScrolledToBottom) scrollToBottom();
    }, [messages]);

    return (
        <div className={classes.root}>
            <div className={`${classes.chatBox} scrollbar`}
                 ref={chatBox}>
                {takeRight(messages,200).map((msg, index) =>
                    <ChatMessage
                        key={`${msg.id}-${index}`}
                        message={msg}
                    />
                )}
            </div>
            <div className={classes.input}>
                <MessageInput onMessage={ message => client.say(room, message)}/>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return { storedRooms: state.storedRooms};
};
export default connect(mapStateToProps)(
    withStyles(styles)(
        ChatRoom
    )
);