import React, { useState } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import VTabBar from "./utility/VTabBar";
import ChatRoom from "./ChatRoom";
import {Tmi, TmiProvider} from "../../model/TmiProvider";
import { withRouter } from "react-router-dom"
import { Fab, Typography } from "@material-ui/core";
import {connect} from "react-redux";
import InputDialog from "../utility/InputDialog";
import ChatChannels, { channels } from "../../model/Channels"
import RoomInfo from "./RoomInfo";
import {useChannelInfoShown} from "../../model/hooks/ChatRoomHooks";

const styles = theme => ({
    root: {
        display: "flex",
        height: "100%",
        backgroundColor: theme.palette.background.paper,
        zIndex: theme.zIndex.appBar,
        minHeight: 0,
    },
    tabBar: {
      flexGrow: 0
    },
    chat: {
        flexGrow: 1,
        minWidth: 0,
    },
    infoBar: {
        flexGrow: 0
    },
    addBtn: {
        margin: "10px",
        border: "2px solid #fff",
        backgroundColor: theme.palette.background.paper,
        transition: "transform .3s",
        '&:hover': {
            backgroundColor: theme.palette.background.default,
            transform: "rotate(90deg)",
        }
    },
    blank: {
        backgroundColor: theme.palette.background.default,
        borderTopLeftRadius: "20px",
        height: "100%"
    },
    blankText: {
        position: "relative",
        textAlign: "center",
        lineHeight: "60px",
        float: "left",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        opacity: 0.5,
        margin: "20px"
    },
});

function ChatApp(props) {
    const { classes, history, channelsData, match } = props;

    // Redirect if not logged in
    if (!TmiProvider.isLogged())
        history.push("/login");

    // Define state
    const [rooms, setRooms] = useState(TmiProvider.retrieveChannelNames());
    const [dialog, setDialog] = useState(false);
    const infoShown = useChannelInfoShown();

    // Get room context
    const room = match.params.room;
    const page = rooms.indexOf(room);

    // If no channel is open
    const blank = rooms.length === 0 || !room;

    // If the page is not in rooms arr, add it
    if (room && !rooms.includes(room)) setRooms(prev => prev.concat(room));

    // Make sure all rooms are connected
    rooms.filter(ele => ChatChannels.isNotConnected(ele))
        .forEach(ele => channels.join(ele));

    // Retrieve channel thumbnails if available
    const thumbnails = rooms.map(it => {
        if (channelsData && channelsData[it]) return channelsData[it].profile_image_url;
        else return null;
    });

    const onPage = (name) => history.push(`/chat/${name}`);
    const onCloseRoom = () => {
        channels.disconnect(room);
        // Display another room after closing active one
        const newRooms = rooms.filter(it => it !== room);
        setRooms(newRooms);
        history.push(`/chat/${newRooms[0] ? newRooms[0] : ""}`);
    };

    const closeDialog = () => setDialog(false);

    const addBtn = (
        <Fab
            className={classes.addBtn}
            variant={"round"}
            size={"medium"}
            onClick={() => setDialog(true)}
        >
            <Typography variant={"h5"}>+</Typography>
        </Fab>
    );

    // TODO Create VBar sections (channels and whispers)
    return (
        <main className={classes.root}>
            <div className={classes.tabBar}><VTabBar
                tabs={rooms}
                onPageChange={onPage}
                activePage={page}
                lastItem={addBtn}
                // sectionIndexes={[0, rooms.length]}
                // sectionNames={[null, "Whispers"]}
                images={thumbnails}
            /></div>
            <div className={classes.chat}>
                { blank && (
                    <div className={classes.blank}>
                        <Typography variant={"h4"} className={classes.blankText}>Nothing here :( <br/> Open some channel</Typography>
                    </div>
                )}
                { !blank && (
                    <ChatRoom
                        key={room}
                        client={Tmi.client}
                        room={rooms[page]}
                    />
                )}
            </div>
            { (!blank && infoShown) && <div className={classes.infoBar}>
                <RoomInfo
                    room={rooms[page]}
                    onCloseRoom={onCloseRoom}
                />
            </div>}
            <InputDialog open={dialog} onConfirm={(text)=>{
                closeDialog();
                onPage(text);
            }} onClose={closeDialog}/>
        </main>
    )
}

const mapStateToProps = state => {
    return {
        channelsData: state.channels
    };
};
export default withRouter(
    connect(mapStateToProps)(
        withStyles(styles)(ChatApp)
    )
)