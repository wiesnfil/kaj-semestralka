import React, {useState} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import * as PropTypes from "prop-types";
import Tabs from "@material-ui/core/Tabs";
import {Button, CircularProgress, Tab} from "@material-ui/core";
import TwitchEmote from "./utility/TwitchEmote";
import {Animate} from "react-show";
import {useEmotes} from "../../model/hooks/ChatRoomHooks";

const style = theme => ({
    root: {},
    emoteSets: {
        flexGrow: 1,
    },
    emotes: {
        height: "100px",
        overflowY: "scroll",
    },
    emotesLoading: {
        width: "40px",
        margin: "10px auto"
    }
});

function EmoteSelection(props) {
    const { classes, open } = props;

    let { onEmoteClicked } = props;
    if (!onEmoteClicked) onEmoteClicked = () => {};

    // Declare state
    const [emoteTab, setEmoteTab] = useState(0);
    let [ emotes, sets ] = useEmotes();

    // bool if should render emotes set => if loaded
    const emotesReady = emotes && sets.length > 0;

    // If no sets are loaded, set default tab
    if (sets.length === 0) sets = ["Twitch"];

    return (
        <div className={classes.root}>
            <Animate show={open}
                     transitionOnMount
                     preMount
                     start={{height: 0}}
                     style={{ height: "auto" }}
            >
                <div className={classes.emoteSets}>
                    <Tabs
                        value={emoteTab}
                        onChange={((event, value) => setEmoteTab(value))}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                    >{sets.map((setName, index) => (
                        <Tab key={index} label={setName}/>
                    ))}
                    </Tabs>
                </div>
                {emotesReady && <div className={`${classes.emotes} scrollbar`}>

                    {/* Conditional rendering on 'open' because of performance hit of showing ~300 emotes*/}
                    {open && emotes[sets[emoteTab]].map(emote => (
                        <Button
                            variant="text"
                            key={`${emoteTab}-${emote.id}`}
                            onClick={() => onEmoteClicked(emote.code)}
                        >
                            <TwitchEmote
                                id={`${emote.id}`}
                                name={emote.code}
                                size={"normal"}
                            />
                        </Button>
                    ))}
                </div>}
                {!emotesReady && <div className={classes.emotesLoading}>
                    <CircularProgress
                        color={"primary"}
                        size={40}
                    />
                </div>}
            </Animate>
        </div>
    )
}

EmoteSelection.propTypes = {
    open: PropTypes.bool,
    onEmoteClicked: PropTypes.func
};

export default withStyles(style)(EmoteSelection)
