import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import * as PropTypes from "prop-types";
import {connect} from "react-redux";
import {Button, CircularProgress, Divider, Typography} from "@material-ui/core";
import {useChatters} from "../../model/hooks/ChatRoomHooks";
import Tooltip from "@material-ui/core/Tooltip";

const style = theme => ({
    root: {
        display: "flex",
        flexDirection: "column",
        margin: theme.spacing.unit * 1.5,
        marginTop: "0",
        width: theme.spacing.unit * 20,
    },
    loading: {
        width: "40px",
        margin: "20px auto"
    },
    title: {},
    desc: {
        marginLeft: "5px",
    },
    badges: {
        marginTop: "15px"
    },
    badgeIcon: {
        width: "1.8em",
        margin: "3px"
    },
    badgeTooltip: { marginTop: "5px" },
    chatters: {
        marginTop: "15px"
    },
    divider: {
        marginTop: "10px"
    },
});

function RoomInfo(props) {
    const { classes, room, onCloseRoom } = props;

    const channelsData = props.channelsData[room];
    const roomData = props.roomData[room];


    const [chattersCount, chatters] = useChatters(room);

    if (channelsData)
        return (
            <aside className={classes.root}>
                <Typography className={classes.title} variant={"h5"}>
                    {channelsData["display_name"]}
                </Typography>

                <Typography className={classes.desc} variant={"caption"}>
                    {channelsData.description}
                </Typography>

                <div className={classes.badges}>
                    <Typography variant={"subtitle1"}>Sub badges:</Typography>
                    {Object.values(roomData.subBadges).map(badge => (
                        <Tooltip title={badge.description} classes={{ tooltip: classes.badgeTooltip }} key={badge.title}>
                            <img className={classes.badgeIcon} alt={badge.description} src={badge.image_url_4x}/>
                        </Tooltip>
                    ))}
                </div>

                <Typography className={classes.chatters}>
                    {`Chatters: ${chattersCount ? chattersCount : 'Unavailable'}`}
                </Typography>
                <Typography>
                    
                </Typography>
                <Divider className={classes.divider}/>
                <Button color={"secondary"} onClick={onCloseRoom}>Leave channel</Button>
            </aside>
        );
    else return (<div className={classes.root}>
        <div className={classes.loading}><CircularProgress
            color={"primary"}
            size={40}
        /></div>
    </div>)
}

RoomInfo.propTypes = {
    room: PropTypes.string.isRequired
};

const mapStateToProps = state => {
    return {
        channelsData: state.channels,
        roomData: state.storedRooms
    };
};
export default connect(mapStateToProps)(
    withStyles(style)(RoomInfo)
)