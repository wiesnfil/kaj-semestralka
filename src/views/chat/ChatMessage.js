import React from 'react'
import { withStyles } from "@material-ui/core";
import { Badges } from "./utility/Badges";
import { colorForUser } from "../../model/Colors";
import { emotify } from "../../model/Emotes";
import * as PropTypes from "prop-types";

const styles = theme => ({
    root: {
        marginTop: "5px",
        marginBottom: "5px"
    },
    message: {
        color: theme.palette.text.primary,
        fontSize: theme.typography.fontSize,
        lineHeight: "20px",
        wordWrap: "break-word",
        overflowWrap: "break-word",
        wordBreak: "break-word",
    },
    username: {
        fontSize: theme.typography.fontSize * 1.1,
        fontWeight: 600,
    },
});

function ChatMessage(props) {
    const { classes, message } = props;
    const { palette } = props.theme;

    let emoteMessage = emotify(message);
    const color = colorForUser(message.userstate, palette.background.default);

    return (
        <div className={classes.root}>
            {message.userstate.badges && (
                <Badges badges={message.userstate.badges} channel={message.channel.substring(1)}/>
            )}
            <span className={classes.username} style={{color: color}}>
                    {`${message.userstate["display-name"]}: `}
                </span>
            {!message.action && <span className={classes.message}>{emoteMessage}</span> }
            {message.action && <span style={{ color: color }} className={classes.message}>{emoteMessage}</span>}
        </div>
    )
}

ChatMessage.propTypes = {
    message: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true} )(ChatMessage)